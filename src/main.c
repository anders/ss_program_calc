
/*============*/
/*Header Files*/
/*============*/

#include "headers.h"

/*=============*/
/*Main Function*/
/*=============*/

int main(int argc, char * argv[])
{
        if (argc < 3) {
                printf("Två argument, tack. Input-filen och h för html eller t för text.\n");
                return EXIT_FAILURE;
        }

        const char * input_file_path = argv[1];
        FILE * input_file = fopen(input_file_path, "r+");

        double ratio_of_increment = 1.15; // change if needed oWo

        lift_struct lifts;
        parse_input(&lifts, input_file);

        calc_increment(&lifts, ratio_of_increment);

        print_to_files(lifts, (argv[2][0] == 'h'));

        return EXIT_SUCCESS;
}
