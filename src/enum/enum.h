
/*====================*/
/* sample enumeration */
/*====================*/

enum LIFTS {
             PRESS,
             BENCH,
             SQUAT,
             DIDDLY
};

enum ACCESSORIES {
        SKULLCRUSHER,
        ROPE_PUSHDOWN,
        CURL,
        LATERAL_RAISE,
        PULL_UP
};

enum STRETCHES {
        SHOULDER,
        JEFFERSON,
        GROIN,
        SHRUG
};
