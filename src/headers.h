
/*==============================================*/
/*Written by Anders Furufors All Rights Reserved*/
/*==============================================*/

/*===========*/
/*Definitions*/
/*===========*/
#define PI 3.14159265358 // typ
#define NBR_LIFTS 4

/*============*/
/*HEADER FILES*/
/*============*/

/* c standard library headers */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <strings.h>

/* gsl headers */
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/* custom headers */

/* enumerations */
#include "enum/enum.h"

/* structs (note hierarchy and interdependence) */
/*no dependencies*/
typedef struct {
        unsigned char cycles;
        double start[NBR_LIFTS];
        double goals[NBR_LIFTS];
        double * current[NBR_LIFTS];
} lift_struct;

//#include "struct/struct.h"

/*depends on above*/

/* functions */
#include "fnc/fnc_headers.h"
