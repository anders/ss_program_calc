
#include "../headers.h"

void print_monday (lift_struct lifts, unsigned char i, unsigned char html)
{
        const char * file_path =  malloc(sizeof(char) * (8 + 3 + 7 + 5));
        sprintf(file_path, "program/%hhd_monday.html", i + 1);
        FILE * fp = fopen(file_path, "w+");

        print_h2(fp, html, "Måndag");

        print_h3(fp, html, "Sträckning");
        begin_list(fp, html);
        print_stretch(fp, SHOULDER, html);
        end_list(fp, html);

        print_h3(fp, html, "Bänkpress");
        begin_table(fp, html);
        print_bench_max (fp, lifts.current[BENCH][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Accessoar");
        begin_list(fp, html);
        print_accesory(fp, SKULLCRUSHER,  html);
        print_accesory(fp, ROPE_PUSHDOWN, html);
        end_list(fp, html);

        fprintf(fp, "\n");

        return;
}

void print_tuesday (lift_struct lifts, unsigned char i, unsigned char html)
{
        const char * file_path =  malloc(sizeof(char) * (8 + 3 + 8 + 5));
        sprintf(file_path, "program/%hhd_tuesday.html", i + 1);
        FILE * fp = fopen(file_path, "w+");

        print_h2(fp, html, "Tisdag");

        print_h3(fp, html, "Sträckning");
        begin_list(fp, html);
        print_stretch(fp, JEFFERSON, html);
        print_stretch(fp, SHRUG,     html);
        print_stretch(fp, GROIN,     html);
        end_list(fp, html);

        print_h3(fp, html, "Knäböj");
        begin_table(fp, html);
        print_squat (fp, lifts.current[SQUAT][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Press");
        begin_table(fp, html);
        print_press (fp, lifts.current[PRESS][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Accessoar");
        begin_list(fp, html);
        print_accesory(fp, LATERAL_RAISE, html);
        print_accesory(fp, PULL_UP,       html);
        end_list(fp, html);

        fprintf(fp, "\n");

        return;
}

void print_wednesday (lift_struct lifts, unsigned char i, unsigned char html)
{
        const char * file_path =  malloc(sizeof(char) * (8 + 3 + 10 + 5));
        sprintf(file_path, "program/%hhd_wednesday.html", i + 1);
        FILE * fp = fopen(file_path, "w+");

        print_h2(fp, html, "Onsdag");

        print_h3(fp, html, "Sträckning");
        begin_list(fp, html);
        print_stretch(fp, SHOULDER, html);
        end_list(fp, html);

        print_h3(fp, html, "Bänkpress");
        begin_table(fp, html);
        print_bench_pyramid (fp, lifts.current[BENCH][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Accessoar");
        begin_list(fp, html);
        print_accesory(fp, SKULLCRUSHER,  html);
        print_accesory(fp, ROPE_PUSHDOWN, html);
        end_list(fp, html);

        fprintf(fp, "\n");

        return;
}

void print_thursday (lift_struct lifts, unsigned char i, unsigned char html)
{
        const char * file_path =  malloc(sizeof(char) * (8 + 3 + 9 + 5));
        sprintf(file_path, "program/%hhd_thursday.html", i + 1);
        FILE * fp = fopen(file_path, "w+");

        print_h2(fp, html, "Torsdag");

        print_h3(fp, html, "Sträckning");
        begin_list(fp, html);
        print_stretch(fp, JEFFERSON, html);
        print_stretch(fp, SHRUG,     html);
        end_list(fp, html);

        print_h3(fp, html, "Marklyft");
        begin_table(fp, html);
        print_diddly(fp, lifts.current[DIDDLY][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Accessoar");
        begin_list(fp, html);
        print_accesory(fp, CURL,    html);
        print_accesory(fp, PULL_UP, html);
        end_list(fp, html);

        fprintf(fp, "\n");

        return;
}

void print_friday (lift_struct lifts, unsigned char i, unsigned char html)
{
        const char * file_path =  malloc(sizeof(char) * (8 + 3 + 7 + 5));
        sprintf(file_path, "program/%hhd_friday.html", i + 1);
        FILE * fp = fopen(file_path, "w+");

        print_h2(fp, html, "Fredag");

        print_h3(fp, html, "Sträckning");
        begin_list(fp, html);
        print_stretch(fp, SHOULDER, html);
        end_list(fp, html);

        print_h3(fp, html, "Bänkpress");
        begin_table(fp, html);
        print_bench_pyramid (fp, lifts.current[BENCH][i], html);
        end_table(fp, html);

        print_h3(fp, html, "Accessoar");
        begin_list(fp, html);
        print_accesory(fp, SKULLCRUSHER, html);
        print_accesory(fp, ROPE_PUSHDOWN, html);
        end_list(fp, html);

        return;
}
