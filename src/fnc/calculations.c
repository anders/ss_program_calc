
#include "../headers.h"

void parse_input (lift_struct * lifts, FILE * fp)
{
        /* parse file */
        (void) fscanf(fp, "%hhd", &lifts->cycles);
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                (void) fscanf(fp, "%lf", &lifts->start[i]);
        }
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                (void) fscanf(fp, "%lf", &(lifts->goals[i]));
                printf("%f\n", lifts->goals[i]);

        }
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                lifts->current[i] = malloc(sizeof(double) * ( lifts->cycles));
        }
        return;
}


void calc_increment (lift_struct * lifts, double ratio_of_increment)
{
        /* calculate with exponentially decreasing size the increments for each lift and create the program in lifts->current[lift][cycle] */
        double diff[NBR_LIFTS];
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                diff[i] = lifts->goals[i]-lifts->start[i];
        }
        double denominator[NBR_LIFTS];
        double increment[NBR_LIFTS];
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                denominator[i] = 0;
                for (unsigned char j = 0; j < lifts->cycles; j++) {
                        denominator[i] +=  pow(ratio_of_increment, j);
                }
                increment[i] = diff[i]/denominator[i];
        }
        for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                lifts->current[i][0] = lifts->start[i] + increment[i];
        }
        for (unsigned char j = 1; j < lifts->cycles; j++) {
                for (unsigned char i = 0; i < NBR_LIFTS; i++) {
                        lifts->current[i][j] = lifts->current[i][j - 1] + increment[i] * pow(ratio_of_increment, lifts->cycles - j);
                }
        }
        return;
}
