
#include "../headers.h"

void print_press (FILE * fp, double press_orm, unsigned char html)
{

        /* warmup sets */
        print_table_header(fp, html, "Uppvärmning");

        print_row(fp, html, 1, 5, 20);
        print_row(fp, html, 1, 5, 0.6 * press_orm);

        /* main sets */
        print_table_header(fp, html, "Arbetsmängder");

        print_row(fp, html, 5, 5, 0.85 * press_orm);

        /* scooby sets*/
        print_table_header(fp, html, "Scooby sets");

        print_row(fp, html, 2, 5, 0.7 * press_orm);
        print_row(fp, html, 1, 0, 0.6 * press_orm);


        return;
}

void print_bench(FILE * fp, double bench_orm, unsigned char html)
{
        fprintf(fp, "%s", html ? "<h3>\nBänk\n</h3>\n" : "=== Bänk ===\n");
        fprintf(fp, "%s", html ? "<table>\n" : "");
        fprintf(fp, "%s", html ? "<tr>\n" : "");
        fprintf(fp, "%s", html ? "<span style =\"color: #ff0000\">\n" : "");
        fprintf(fp, "%s", html ? "<th>\nSets\n</th>\n" : "");
        fprintf(fp, "%s", html ? "<th>\nReps\n</th>\n" : "");
        fprintf(fp, "%s", html ? "<th>\nVikt\n</th>\n" : "");
        fprintf(fp, "%s", html ? "</span>\n" : "");
        fprintf(fp, "%s", html ? "</tr>\n" : "");

        /* warmup sets */
        fprintf(fp, "%s", html ? "<tr>\n" : "= ");
        fprintf(fp, "%s", html ? "<td>\n" : "");
        fprintf(fp, "%s", html ? "<span style =\"color: #ff0000\">\n" : "");
        fprintf(fp, "%s", html ? "Uppvärmning\n" : "");
        fprintf(fp, "%s", html ? "</span>\n" : "");
        fprintf(fp, "%s", html ? "</td>\n" : "");
        fprintf(fp, "%s", html ? "</tr>\n" : "");

        print_row(fp, html, 1, 10, 20);
        print_row(fp, html, 1, 5, bench_orm * 0.6);

        /* main sets */
        fprintf(fp, "= Arbetsmängder:\n");
        fprintf(fp, " 5x5  @ %.0f kg\n", bench_orm * 0.8);

        /* Scooby sets */
        fprintf(fp, "= Scooby sets:\n");
        fprintf(fp, " 2x5  @ %.0f kg\n", bench_orm * 0.7);
        fprintf(fp, " 1xf  @ %.0f kg\n", bench_orm * 0.6);


        return;
}

void print_bench_max (FILE * fp, double bench_orm, unsigned char html)
{

        /* warmup sets */
        print_table_header(fp, html, "Uppvärmning");

        print_row(fp, html, 1, 10, 20);
        print_row(fp, html, 1, 5, 0.6 * bench_orm);
        print_row(fp, html, 1, 3, 0.8 * bench_orm);
        print_row(fp, html, 1, 1, 0.9 * bench_orm);

        /* main sets */
        print_table_header(fp, html, "Arbetsmängder");

        print_row(fp, html, 1, 1, 1 * bench_orm);

        /* Scooby sets */
        print_table_header(fp, html, "Scooby sets");

        print_row(fp, html, 4, 10, 0.8 * bench_orm);
        print_row(fp, html, 1, 0, 0.6 * bench_orm);


        return;
}

void print_bench_pyramid (FILE * fp, double bench_orm, unsigned char html)
{

        /* warmup sets */
        print_table_header(fp, html, "Uppvärmning");

        print_row(fp, html, 1, 10, 20);

        /* main sets */
        print_table_header(fp, html, "Arbetsmängder");

        print_row(fp, html, 1, 20, 0.5 * bench_orm);
        print_row(fp, html, 1, 15, 0.6 * bench_orm);
        print_row(fp, html, 1, 10, 0.7 * bench_orm);
        print_row(fp, html, 2,  5, 0.8 * bench_orm);
        print_row(fp, html, 1, 10, 0.7 * bench_orm);
        print_row(fp, html, 1, 15, 0.6 * bench_orm);
        print_row(fp, html, 1, 20, 0.5 * bench_orm);


        return;
}

void print_squat (FILE * fp, double squat_orm, unsigned char html)
{

        /* warmup sets */
        print_table_header(fp, html, "Uppvärmning");

        print_row(fp, html, 1, 10, 20);
        print_row(fp, html, 1, 10, 0.6 * squat_orm);

        /* main sets */
        print_table_header(fp, html, "Arbetsmängder");

        print_row(fp, html, 1, 10, 0.67 * squat_orm);
        print_row(fp, html, 3, 10, 0.75 * squat_orm);
        print_row(fp, html, 1, 10, 0.67 * squat_orm);

        /* Scooby sets */
        print_table_header(fp, html, "Scooby sets");

        print_row(fp, html, 2, 10, 0.6 * squat_orm);


        return;
}

void print_diddly (FILE * fp, double diddly_orm, unsigned char html)
{
        /* warmup sets */
        print_table_header(fp, html, "Uppvärmning");

        print_row(fp, html, 1, 5, 0.4 * diddly_orm);
        print_row(fp, html, 1, 5, 0.6 * diddly_orm);

        /* main sets*/
        print_table_header(fp, html, "Arbetsmängder");

        print_row(fp, html, 1, 5, 0.8 * diddly_orm);
        print_row(fp, html, 3, 5, 0.9 * diddly_orm);
        print_row(fp, html, 1, 5, 0.8 * diddly_orm);

        /* Scooby sets */
        print_table_header(fp, html, (char *) "Scooby sets");

        print_row(fp, html, 1, 5, 0.7 * diddly_orm);
        print_row(fp, html, 1, 5, 0.6 * diddly_orm);


        return;
}

void print_accesory (FILE * fp, unsigned char accesory, unsigned char html)
{
        switch (accesory) {

        case SKULLCRUSHER:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Skallkrossare\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case ROPE_PUSHDOWN:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Repnedåtskjut\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case CURL:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Krullningar\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case LATERAL_RAISE:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Sidohöjningar\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case PULL_UP:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Räckhäv\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        default:
                break;
        }

        return;
}

void print_stretch (FILE * fp, unsigned char stretch, unsigned char html)
{

        switch (stretch) {

        case SHOULDER:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Axlar med pinne\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case JEFFERSON:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Jeffersonska krullningar\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case GROIN:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Sitta med knän i sär\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Sitta med rygg rak\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Sitta med pinne över huvudet\n");                fprintf(fp, "%s", html ? "</li>\n" : "");

                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Sitta och sträcka sig uppåt\n");
                fprintf(fp, "%s", html ? "</li>\n" : "");
                break;

        case SHRUG:
                fprintf(fp, "%s", html ? "<li>\n" : "= ");
                fprintf(fp, "Räckhäv med axelryckning\n");
                break;
                fprintf(fp, "%s", html ? "</li>\n" : "");

        default:
                break;
        }

        return;
}
