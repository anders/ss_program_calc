
#include "../headers.h"

void print_to_files(lift_struct lifts, unsigned char html)
{
        for (unsigned char i = 0; i < lifts.cycles; i++) {
                print_cycle(lifts, i, html);
        }
        return;
}

void print_cycle(lift_struct lifts, unsigned char i, unsigned char html)
{
        print_monday(lifts, i, html);
        print_tuesday(lifts, i, html);
        print_wednesday(lifts, i, html);
        print_thursday(lifts, i, html);
        print_friday(lifts, i, html);

        return;
}

void print_row (FILE * fp, unsigned char html, unsigned char sets, unsigned char reps, double vikt)
{
        fprintf(fp, "%s", html ? "<tr>\n" : "= ");
        fprintf(fp, "%s", html ? "<td style=\"text-align: center\">\n" : "");
        fprintf(fp, "%d", sets);
        fprintf(fp, "%s", html ? "\n" : "x");
        fprintf(fp, "%s", html ? "</td>\n" : "");
        fprintf(fp, "%s", html ? "<td style=\"text-align: center\">\n" : "");
        fprintf(fp, "%d", reps);
        fprintf(fp, "%s", html ? "\n" : " @ ");
        fprintf(fp, "%s", html ? "</td>\n" : "");
        fprintf(fp, "%s", html ? "<td style=\"text-align: center\">\n" : "");
        fprintf(fp, "%.0f kg\n", vikt);
        fprintf(fp, "%s", html ? "</td>\n" : "");

        return;
}

void print_table_header (FILE * fp, unsigned char html, char * header)
{
        fprintf(fp, "%s", html ? "<tr>\n" : "== ");
        fprintf(fp, "%s", html ? "<td colspan=\"3\">\n" : "");
        fprintf(fp, "%s", html ? "<span style =\"color: #ffd700\">\n" : "");
        fprintf(fp, "%s\n", header);
        fprintf(fp, "%s", html ? "</span>\n" : "");
        fprintf(fp, "%s", html ? "</td>\n" : "");
        fprintf(fp, "%s", html ? "</tr>\n" : "");
        fprintf(fp, "%s", html ? "<tr>\n" : "");
        fprintf(fp, "%s", html ? "<th>\n<span style =\"color: #ff0000\">\nSets\n</span>\n</th>\n" : "");
        fprintf(fp, "%s", html ? "<th>\n<span style =\"color: #ff0000\">\nReps\n</span>\n</th>\n" : "");
        fprintf(fp, "%s", html ? "<th>\n<span style =\"color: #ff0000\">\nVikt\n</span>\n</th>\n" : "");
        fprintf(fp, "%s", html ? "</tr>\n" : "");


        return;
}

void print_h2 (FILE * fp, unsigned char html, char * header)
{
        if (html) {
                fprintf(fp, "<h2>\n%s\n</h2>\n", header);
        }
        else {
                fprintf(fp, "=*=*=*= %s =*=*=*=\n\n", header);
        }
        return;
}

void print_h3 (FILE * fp, unsigned char html, char * header)
{
        if (html) {
                fprintf(fp, "<h3>\n%s\n</h3>\n", header);
        }
        else {
                fprintf(fp, "=== %s ===\n", header);
        }
        return;
}

void begin_list (FILE * fp, unsigned char html)
{
        fprintf(fp, "%s", html ? "<ul>\n" : "");
        return;
}

void end_list (FILE * fp, unsigned char html)
{
        fprintf(fp, "%s", html ? "</ul>\n" : "\n");
        return;
}

void begin_table (FILE * fp, unsigned char html)
{
        fprintf(fp, "%s", html ? "<table>\n" : "");
        return;
}

void end_table (FILE * fp, unsigned char html)
{
        fprintf(fp, "%s", html ? "</table>\n" : "\n");
        return;
}
