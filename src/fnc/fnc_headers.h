
/*========================*/
/* function category      */
/*========================*/

/* calculation backend */
extern void parse_input (lift_struct * lifts, FILE * fp);
extern void calc_increment (lift_struct * lifts, double ratio_of_increment);

/* printing functions */
extern void print_to_files(lift_struct lifts, unsigned char html);
extern void print_cycle(lift_struct lifts, unsigned char i, unsigned char html);
extern void print_table_header (FILE * fp, unsigned char html, char * header);
extern void print_h2 (FILE * fp, unsigned char html, char * header);
extern void print_h3 (FILE * fp, unsigned char html, char * header);
extern void begin_list (FILE * fp, unsigned char html);
extern void end_list (FILE * fp, unsigned char html);
extern void begin_table (FILE * fp, unsigned char html);
extern void end_table (FILE * fp, unsigned char html);
extern void print_row (FILE * fp, unsigned char html, unsigned char sets, unsigned char reps, double vikt);

/* Övningar */
extern void print_press (FILE * fp, double press_orm, unsigned char html);
extern void print_bench(FILE * fp, double bench_orm, unsigned char html);
extern void print_bench_max(FILE * fp, double bench_orm, unsigned char html);
extern void print_bench_pyramid(FILE * fp, double bench_orm, unsigned char html);
extern void print_squat (FILE * fp, double squat_orm, unsigned char html);
extern void print_diddly (FILE * fp, double diddly_orm, unsigned char html);
extern void print_accesory (FILE * fp, unsigned char accesory, unsigned char html);
extern void print_stretch (FILE * fp, unsigned char stretch, unsigned char html);

/* Dagar */
extern void print_monday (lift_struct lifts, unsigned char i, unsigned char html);
extern void print_tuesday (lift_struct lifts, unsigned char i, unsigned char html);
extern void print_wednesday (lift_struct lifts, unsigned char i, unsigned char html);
extern void print_thursday (lift_struct lifts, unsigned char i, unsigned char html);
extern void print_friday (lift_struct lifts, unsigned char i, unsigned char html);
